from django.shortcuts import render
from .models import Product, Category, Order, Customer
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages #kinda popup alert
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .forms import SignUpForm #from our forms.py
from django import forms



#PRODUCT
#Home_Page : Get all products
def home(request):
    products = Product.objects.all()
    return render(request, 'store/home.html', {'products' : products})


#Single Product view
def product(request, pk):
    product = Product.objects.get(id=pk)
    return render(request, 'store/product.html', {'product':product})



#CATEGORY
def category(request, foo):
    foo = foo.replace('-', ' ') #Replace '-' with space

    #grab the category from the url
    try:
        category = Category.objects.get(name=foo)
        products = Product.objects.filter(category = category)
        return render(request, 'store/category.html', {'products':products, 'category':category})
    except:
        messages.error(request, ('That category does not exist'))
        return redirect('home')


def about(request):
    return render(request, 'store/about.html', {})


def login_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password'] #the name attribute we gave into the html page
        user = authenticate(request, username = username, password = password)
        if user is not None:
            login(request, user)
            messages.success(request, ("You have been logged in successfully !"))
            return redirect('home')
        else:
            messages.error(request, ("Login failed ! Please TRY again ..."))
            return redirect('login')
    else:
        return render(request, 'store/login.html', {})

def logout_user(request):
    logout(request)
    messages.success(request, ('You have been logged out... See u soon !'))
    return redirect('home')

def register_user(request):
    form = SignUpForm()
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']

            #Log in  user
            user = authenticate(username = username, password = password)
            login(request, user)
            messages.success(request, ("You have registered successfully! Welcome !!"))
            return redirect('home')
        else:
            messages.error(request, 'OUPS ! There was a problem while registering, please try again ... ')
            return redirect('register')
    else:
        return render(request, 'store/register.html', {'form':form})

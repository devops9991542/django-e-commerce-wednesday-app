from .cart import Cart


#Let's create context processor so that our cart can work on all pages of the website

def cart(request):
    #Return the default data from our Cart
    return {'cart': Cart(request)}

class Cart():
    def __init__(self, request):
        self.session = request.session


        #Get the current session key (Cookies' stuffs)
        cart = self.session.get('session_key')

        #If the user is new => no session key ! Let's create One
        if 'session_key' not in request.session:
            cart = self.session['session_key'] = {}


        #Make sure that cart is available on all pages of website
        self.cart = cart